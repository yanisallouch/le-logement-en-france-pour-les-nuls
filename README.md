# Logement en France

1. [Logement en France](#logement-en-france)
	1. [Contexte](#contexte)
	2. [Description](#description)
		1. [Location immobilière](#location-immobilière)
		2. [Aides financières](#aides-financières)
		3. [Protection et sécurité de l'habitat](#protection-et-sécurité-de-lhabitat)
		4. [Vie pratique dans un logement](#vie-pratique-dans-un-logement)
	3. [Contrat de location (bail)](#contrat-de-location-bail)
		1. [Ma situation](#ma-situation)
		2. [Rédaction du bail d'habitation (contrat de location) - Logement meublé](#rédaction-du-bail-dhabitation-contrat-de-location-logement-meublé)
		3. [Clauses interdites dans un contrat de location - Logement meublé](#clauses-interdites-dans-un-contrat-de-location-logement-meublé)
		4. [Dépôt de garantie dans un bail d'habitation](#dépôt-de-garantie-dans-un-bail-dhabitation)
		5. [État des lieux d'entrée dans un bail d'habitation](#état-des-lieux-dentrée-dans-un-bail-dhabitation)

## Contexte

Ce document a pour vocation d'aider à comprendre comment est structuré un bail, autrement désigné un contrat de location immobilière. Notammenet, ce qui est obligatoire et illégal dans sa composition. Il se base sur la documentation légale mise en service par le gouvernements, sur les liens <https://service-public.fr> officiel et à jour.

Par ailleurs, je souhaite inclure des données du bail qui m'est fourni pour un logement que j'ai ciblé. Ces données me permettront de fournir un exemple afin de facilité la compréhension des règles mais aussi afin de juger si le bail fourni est légal. J'essaierai autant que possible de mettre en surbrillance ( **gras** / *italique* / `bloc` ) les parties du document indiquant des données fournies dans le bail ou mail officiel de l'agence en charge du dossier.

## Description

"Le site officiel de l'administration française" publie sur une page intitulé "Logement", plusieurs sections en lien avec ce domaine. De l'achat d'un logement à la vie pratique en logement, un ensemble de sujet pratiquo-légal est couvert. Cette page est accessible a l'adresse suivante <https://www.service-public.fr/particuliers/vosdroits/N19808>.

### Location immobilière

De cette page d'acceuil, plusieurs sections retiennent mon attention dans l'objectif de louer un logement dans le parc privé. Ainsi, l'énumération suivante ordonne les sujets a traiter par ordre de priorité (le plus important en premier) selon notre contexte en lien avec la section "Location immobilière" :

1. Contrat de location (bail)
   - <https://www.service-public.fr/particuliers/vosdroits/N349>
2. Obligations du locataire
   - <https://www.service-public.fr/particuliers/vosdroits/N19424>
3. Obligations du locataire
   - <https://www.service-public.fr/particuliers/vosdroits/N31059>
4. Loyer
   - <https://www.service-public.fr/particuliers/vosdroits/N337>
5. Devenir locataire d'un logement privé
   - <https://www.service-public.fr/particuliers/vosdroits/N292>
6. Fin du bail
   - <https://www.service-public.fr/particuliers/vosdroits/N339>
7. Accéder à un logement social
   - <https://www.service-public.fr/particuliers/vosdroits/N31802>

Cependant d'autres sections sont aussi intérréssante et nécéssite une lecture. De la même manière, les énumérations suivantes ordonnes par ordre de priorité (le plus important en premier) les pages à lire.

### Aides financières

1. Aides personnelles au logement
   - <https://www.service-public.fr/particuliers/vosdroits/N20360>
2. Aide pour le dépôt de garantie ou la caution d'un logement en location
   - <https://www.service-public.fr/particuliers/vosdroits/N20359>
3. Aides au paiement des factures : eau, téléphone, électricité, gaz
   - <https://www.service-public.fr/particuliers/vosdroits/N23557>
4. Aides et prêts pour l'amélioration et la rénovation énergétique de l'habitat
   - <https://www.service-public.fr/particuliers/vosdroits/N321>

### Protection et sécurité de l'habitat

1. Risques sanitaires et sécurité du logement
   - <https://www.service-public.fr/particuliers/vosdroits/N20356>
2. Diagnostic immobilier
   - <https://www.service-public.fr/particuliers/vosdroits/N20591>

### Vie pratique dans un logement

1. Vie pratique dans sa maison
   - <https://www.service-public.fr/particuliers/vosdroits/N31030>
2. Nuisances de voisinage
   - <https://www.service-public.fr/particuliers/vosdroits/N356>
3. Déchets
   - <https://www.service-public.fr/particuliers/vosdroits/N31125>

## Contrat de location (bail)

Cette première partie s'intérrèsse a la construction d'un contrat de location (bail). On retrouve 4 grandes sections ordonnée logiquement de la manière suivante :

1. Rédaction du contrat
   - <https://www.service-public.fr/particuliers/vosdroits/F920>
1. Clauses abusives
   - <https://www.service-public.fr/particuliers/vosdroits/F1686>
1. Dépôt de garantie
   - <https://www.service-public.fr/particuliers/vosdroits/F31269>
1. État des lieux d'entrée
   - <https://www.service-public.fr/particuliers/vosdroits/F31270>

### Ma situation

C'est un logement loué meublé. Le bail sera signé après le 1 novembre 2022.

<a id="rédaction-du-bail-dhabitation-contrat-de-location-logement-meublé"></a>

### Rédaction du bail d'habitation (contrat de location) - Logement meublé

La documentation fournie nous redirige vers d'autres documentations. Voir le tableau suivant :

| Intitulé                                                                                                                                          | URL                                                          |
|-------------------------------------------------------------------------------------------------------------------------------------------------- |------------------------------------------------------------- |
| Location meublée ou vide : quelles différences ?                                                                                                  | <https://www.service-public.fr/particuliers/vosdroits/F1165>   |
| Peut-on se rétracter après avoir signé un bail d'habitation ?                                                                                     | <https://www.service-public.fr/particuliers/vosdroits/F1703>   |
| Charges à payer par le locataire (charges "locatives" ou "récupérables")                                                                          | <https://www.service-public.fr/particuliers/vosdroits/F947>    |
| Frais d'agence immobilière à la location d'un logement d'habitation                                                                               | <https://www.service-public.fr/particuliers/vosdroits/F375>    |
| Diagnostic immobilier : diagnostic de performance énergétique (DPE)                                                                               | <https://www.service-public.fr/particuliers/vosdroits/F16096>  |
| Diagnostic immobilier : constat de risque d'exposition au plomb (Crep)                                                                            | <https://www.service-public.fr/particuliers/vosdroits/F1142>   |
| Diagnostic immobilier : état des risques et pollutions                                                                                            | <https://www.service-public.fr/particuliers/vosdroits/F12239>  |
| Diagnostic immobilier : état de l'installation intérieure d'électricité                                                                           | <https://www.service-public.fr/particuliers/vosdroits/F18692>  |
| Diagnostic immobilier : état de l'installation intérieure de gaz                                                                                  | <https://www.service-public.fr/particuliers/vosdroits/F17337>  |
| Diagnostic immobilier : état d'amiante ou "diagnostic amiante"                                                                                    | <https://www.service-public.fr/particuliers/vosdroits/F742>    |
| Arrêté du 29 mai 2015 relatif au contenu de la notice d'information annexée aux contrats de location de logement à usage de résidence principale  | <https://www.legifrance.gouv.fr/loda/id/JORFTEXT000030649902>  |
| État des lieux d'entrée dans un bail d'habitation                                                                                                 | <https://www.service-public.fr/particuliers/vosdroits/F31270>  |
| État des lieux de sortie pour un bail d'habitation                                                                                                | <https://www.service-public.fr/particuliers/vosdroits/F33671>  |
| Assurance habitation du locataire : risques locatifs                                                                                              | <https://www.service-public.fr/particuliers/vosdroits/F1349>   |
| Logement à louer : dans quel cas signer une convention avec l'Anah ?                                                                              | <https://www.service-public.fr/particuliers/vosdroits/F1351>   |
| Entretien courant et "réparations locatives" à la charge du locataire                                                                             | <https://www.service-public.fr/particuliers/vosdroits/F31697>  |
| Règlement de copropriété                                                                                                                          | <https://www.service-public.fr/particuliers/vosdroits/F2589>   |
| Montpellier : estimer les loyers de référence (bail signé après juin 2022) (Outil de recherche)                                                   | <https://www.service-public.fr/particuliers/vosdroits/R62655>  |

<a id="clauses-interdites-dans-un-contrat-de-location-logement-meublé"></a>

### Clauses interdites dans un contrat de location - Logement meublé

La documentation fournie nous redirige vers d'autres documentations. Voir le tableau suivant :

| Intitulé                                                                         | URL                                                          |
|--------------------------------------------------------------------------------- |------------------------------------------------------------- |
| Doit-on payer des frais au propriétaire en cas de retard de paiement du loyer ?  | <https://www.service-public.fr/particuliers/vosdroits/F2889>   |
| Copropriété : qu'est-ce que le règlement de l'immeuble ?                         | <https://www.service-public.fr/particuliers/vosdroits/F33877>  |
| Peut-on faire payer les frais d'état des lieux au locataire ?                    | <https://www.service-public.fr/particuliers/vosdroits/F10696>  |
| Obligation du locataire : assurance habitation                                   | <https://www.service-public.fr/particuliers/vosdroits/F31300>  |
| Le locataire doit-il accorder un droit de visite à son propriétaire ?            | <https://www.service-public.fr/particuliers/vosdroits/F1857>   |

Voici les points d'attentions qui sont interdits :

| Questions  | Réponses  |
|--- |--- |
| Impose au locataire de souscrire en plus du contrat de bail, un contrat pour la location d'équipements ?  |   |
| Autorise le propriétaire à diminuer ou à supprimer, sans contrepartie équivalente, des prestations prévues au contrat de bail ?  |   |
| Le propriétaire ne peut pas imposer à son locataire des frais en plus du paiement du loyer et des charges ?  |   |
| Impose le prélèvement automatique ou la signature par avance de traites ou de billets à ordre comme mode de paiement du loyer ?  |   |
| Stipule que le locataire autorise le propriétaire à prélever ou à faire prélever les loyers directement sur son salaire dans la limite cessible ?  |   |
| Autorise le propriétaire à percevoir des amendes et pénalités en cas d'infractions aux clauses du contrat ou au règlement intérieur de l'immeuble ?  |   |
| Impose au locataire la facturation de l'état des lieux de sortie (toutefois, en cas de recours à un commissaire de justice, le locataire est redevable de la moitié des frais) ?  |   |
| Impose au locataire le versement, lors de l'entrée dans les lieux, de sommes d'argent en plus de celles prévues (dépôt de garantie et rémunération des personnes qui interviennent pour établir un acte de location, c'est l'exemple des frais notariés) ?  |   |
| Fait supporter au locataire des frais de relance ou d'expédition de la quittance ?  |   |
| Engage le locataire par avance à des remboursements sur la base d'une estimation faite uniquement par le propriétaire pour les réparations locatives ?  |   |
| Une clause qui oblige le locataire à prendre une assurance auprès d'une compagnie choisie par le propriétaire est interdite ?  |   |
| interdit au locataire l'exercice d'une activité politique, syndicale, associative ou confessionnelle ?  |   |
| interdit au locataire d'héberger des personnes ne vivant pas habituellement avec lui ?  |   |
| interdit au locataire de demander une indemnité au propriétaire lorsque ce dernier réalise des travaux d'une durée supérieure à 21 jours est interdite ?  |   |
| Le contrat de bail doit être équilibré. Le propriétaire ne peut ainsi s'exonérer de sa responsabilité ou faire peser sur son locataire une responsabilité systématique ?  |   |
| interdit de prévoir la responsabilité collective des locataires en cas de dégradation d'un élément commun de la chose louée  |   |
| d'interdire au locataire de rechercher la responsabilité du propriétaire ou d'exonérer le propriétaire de toute responsabilité  |   |
| prévoir que le locataire est automatiquement responsable des dégradations constatées dans le logement  |   |
| une clause qui prévoit que ces visites aient lieu les jours fériés ou durant plus de 2 heures les jours ouvrables: Correspond à tous les jours de la semaine, à l'exception du jour de repos hebdomadaire (généralement le dimanche) et des jours fériés habituellement non travaillés dans l'entreprise est interdite.  |   |
| Le propriétaire n'a pas le droit de prévoir une clause de résiliation du bail qui aurait pour effet de contourner la réglementation en vigueur.  |   |
| prévoir la résiliation du contrat pour d'autres motifs que le non-paiement du loyer ou des charges, du dépôt de garantie, la non souscription d'une assurance pour risques locatifs, ou les troubles de voisinage constatés par le juge  |   |
| permettre au propriétaire d'obtenir la résiliation du bail au moyen d'une simple ordonnance de référé que le locataire ne pourrait pas contester  |   |

### Dépôt de garantie dans un bail d'habitation

La documentation fournie nous redirige vers d'autres documentations. Voir le tableau suivant :

| Intitulé                                                             | URL                                                          |
|--------------------------------------------------------------------- |------------------------------------------------------------- |
| Avance Loca-Pass : aide au locataire pour le dépôt de garantie       | <https://www.service-public.fr/particuliers/vosdroits/F18490>  |
| Quelle aide apporte le fonds de solidarité pour le logement (FSL) ?  | <https://www.service-public.fr/particuliers/vosdroits/F1334>   |

Attention, le montant du dépôt de garantie ne peut faire l'objet d'aucune révision en cours ou au renouvellement du bail !

Dans le cadre d'un loyer mensuel :

| Questions  | Réponses  |
|--- |--- |
| son montant doit obligatoirement être indiqué dans le contrat de location (bail),  |   |
| son montant ne doit pas être supérieur à 2 mois de loyer, hors charges.  |   |
| exiger du propriétaire qu'il lui remette un reçu attestant que la somme remise correspond au dépôt de garantie.  |   |
| En fin de bail, le dépôt de garantie doit être restitué dans un délai maximal de 1 mois  |   |
| Le propriétaire doit justifier la retenue sur le dépôt de garantie en remettant au locataire des documents (états des lieux, des photos, constat du commissaire, devis, facture, lettre de réclamation des loyers impayés sans réponse.  |   |
| Les provisions sur charge (hors forfait) ne dépassent pas 20 % du montant du dépôt de garantie.  |   |
| La régularisation définitive et la restitution du solde (déduction faite des retenues) doivent être faites dans le mois qui suit l'approbation définitive des comptes de l'immeuble.  |   |

### État des lieux d'entrée dans un bail d'habitation

Voici ce qu'il faut vérifier :

| Questions  | Réponses  |
|--- |--- |
| L'état des lieux ce fait avec le propriétaire (ou mandaté) et le locataire ?  |   |
| L'état des lieux se fait dans de bonnes conditions d'éclairage ?  |   |
| Le logement contient les équipements mentionnées dans le bail ?  |   |
| Les documents sont établis en deux exemplaires (locataire/propriétaire) ?  |   |
| La forme du document permet la comparaison de l'état du logement constaté à l'entrée et à la sortie des lieux ?<br>*Sur un document unique, comportant pour chaque pièce du logement une colonne "à l'entrée du locataire"et une colonne "à la sortie du locataire".<br>* Ou sur des documents distincts ayant une présentation similaire.  |   |
| Le type d'état est indiqué ? (entrée/sortie)  |   |
| La date d'établissement est indiquée ?  |   |
| La localisation est indiquée ?  |   |
| Le nom/dénomination des parties est indiqué ?  |   |
| Le domicile ou siège social du bailleur est indiqué ?  |   |
| Si applicable (charges non payées au forfait), relevés des compteurs individuels d'eau ou d'énergie est indiqué ?<br>* Si non-branchement des compteurs d'eau, de gaz ou d'électricité, alors émettre une réserve sur le document.  |   |
| Clés ou autre moyen d'accès aux locaux à usage privé ou commun est indiquée ?  |   |
| Pour chaque pièce et partie du logement, une description précise de l'état est indiquée :<br>- des revêtements, des sols, murs et plafonds, des équipements<br>- et des éléments du logement ?<br>* Elle peut être complétée d'observations ou de réserves et illustré d'images.  |   |
| Chaque partie (locataire/bailleur ou mandatées) a signé le document ?   |   |
| Les frais de l'état des lieux effectué par l'intermédiaire d'un professionnel (agent immobilier...) ne coutent pas plus pour le locataire :<br>- de la moitié des frais totaux,<br>- un maximum de 3€/m² (TTC) de surface habitable ?  |   |
